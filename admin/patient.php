<?php
session_start();
include_once '../assets/conn/dbconnect.php';
// include_once 'connection/server.php';
if(!isset($_SESSION['adminSession']))
{
header("Location: ../index.php");
}
$usersession = $_SESSION['adminSession'];
$res=mysqli_query($con,"SELECT * FROM admin WHERE adminId=".$usersession);
$userRow=mysqli_fetch_array($res,MYSQLI_ASSOC);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="images/logoo.png">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
    </head>
    <body >
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <div class="navbar-nav divnav">
                <span class="span1">
                    <img class="navlogo" src="images/logoo.png" >
                </span>
                <a class='nav-item nav-link' href='admindashbord.php?bouton2=medecin'><b>medecin</b></a>
                <a class='nav-item nav-link active' href='#'><b>patient</b></a>
                <a class='nav-item nav-link' href='profil.php?bouton1=profil'><b>Profil</b></a>
                <a id="aout" class="nav-item nav-link" href='patient.php?bouton5=logout'><i class="fa fa-sign-out" aria-hidden="true"> </i></a>
            </div>
        </nav>

    <header class="hdr">
        <br>
        <h3>Patients :</h3>
        <center>
        <?php
            if (!empty($_GET['msg'])) {
                echo "<span class='spanmsg'>".$_GET['msg']."</span><br><br>";
            }
        ?>
        </center>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Nom </th>
              <th scope="col">prenom </th>
              <th scope="col">Email</th>
              <th scope="col">Telephone</th>
              <th scope="col">Operation</th>
            </tr>
          </thead>
          <tbody>
            <?php
                
                $requet1="SELECT * FROM patient ";
                $query1 = mysqli_query($con,$requet1) or die(mysqli_error($con)); 
                foreach ($query1 as $value1) {

                echo"<tr>
                      <td>".$value1['icPatient']."</td>
                      <td>".$value1['patientFirstName']."</td>
                      <td>".$value1['patientLastName']."</td>
                      <td>".$value1['patientEmail']."</td>
                      <td>".$value1['patientPhone']."</td>
                      <input type='hidden' name='autorisation' value='".$value1['autorisation']."'>
                      ";
                        
                        if ($value1['autorisation']==1) {
                            echo"<td><a class='bt12' id='bloque' href='Bloquer.php?id=".$value1['icPatient']."&btnclient=bloque'><i class='fa fa-minus-circle' aria-hidden='true'> </i> Bloquer</a></td>
                            "
                            ;
                        }elseif ($value1['autorisation']==0) {
                            echo"<td><a class='bt12n' id='debloque' href='Bloquer.php?id=".$value1['icPatient']."&btnclient=debloque'><i class='fa fa-minus-circle' aria-hidden='true'> </i> Debloquer</a></td>
                            ";
                        }
                }
            ?>
          </tbody>
        </table>
    
    </header>
</header>
    </body>
</html>
<?php

    if(!empty($_GET['bouton5'])){
        header('location:../index.php');
        session_destroy();
    }
?>