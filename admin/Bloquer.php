<?php
include_once '../assets/conn/dbconnect.php';
session_start();
$usersession = $_SESSION['adminSession'];

//Ajouter un medecin
if(isset($_POST['ajoutermed'])){

		$id = $_POST['doctorId'];
		$nom = $_POST['nommed'];
		$pnom = $_POST['prenom'];
		$mail = $_POST['mail'];
		$psw = $_POST['password'];
		$tel = $_POST['phone'];
		$adrs=$_POST['adresse'];
		$today = date("Y-m-d");
		$lien = $_POST['lien'];

		$requet="INSERT INTO doctor (doctorId,doctorFirstName,doctorLastName,doctorEmail,password,doctorPhone,doctorAddress,doctorDOB,link) VALUES ('$id','$nom','$pnom','$mail','$psw','$tel','$adrs','$today','$lien')";
		$query = mysqli_query($con,$requet) or die(mysqli_error($con)); 
            $msg="Medecin est bien ajouter";
			header("location:admindashbord.php?msg=".$msg);
	  }

//Bloquer ou debloquer des clients
if (!empty($_GET['btnclient'])) {
		$id = $_GET['id'];

		if ($_GET['btnclient']=="bloque") {

		 	$requet1 ="UPDATE patient SET autorisation='0' WHERE icPatient=".$id;
		 	$query1 = mysqli_query($con,$requet1) or die(mysqli_error($con));

		 	$msg="utilisateur a ete bloque";
		 	header("location:patient.php?msg=".$msg);

        }elseif ($_GET['btnclient']=="debloque") {

		 	$requet1 ="UPDATE patient SET autorisation='1' WHERE icPatient=".$id;
		 	$query1 = mysqli_query($con,$requet1) or die(mysqli_error($con));

		 	$msg="utilisateur a ete debloque";
		 	header("location:patient.php?msg=".$msg);
        }
    }

//Bloquer ou debloquer des medecins
if (!empty($_GET['btnmed'])) {
		$id = $_GET['doctorId'];

		if ($_GET['btnmed']=="bloque") {

		 	$requet1 ="UPDATE doctor SET autorisation='0' WHERE doctorId=".$id;
		 	$query1 = mysqli_query($con,$requet1) or die(mysqli_error($con));

		 	$msg="Medecin a ete bloque";
		 	header("location:admindashbord.php?msg=".$msg);

        }elseif ($_GET['btnmed']=="debloque") {

		 	$requet1 ="UPDATE doctor SET autorisation='1' WHERE doctorId=".$id;
		 	$query1 = mysqli_query($con,$requet1) or die(mysqli_error($con));

		 	$msg="Medecin a ete debloque";
		 	header("location:admindashbord.php?msg=".$msg);
        }
    }