<?php 
include_once '../assets/conn/dbconnect.php';
 session_start();
$usersession = $_SESSION['adminSession'];
$res=mysqli_query($con,"SELECT * FROM admin WHERE adminId=".$usersession);
foreach ($res as $value) {

?>

 <!DOCTYPE html>
<html>
	<head>
		<title>Profile</title>
		<meta charset="utf-8">
		<meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
     
	    <link rel="shortcut icon" type="image/png" href="images/logoo.png">
	    <link rel="stylesheet" href="css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/bootstrap.css">
	    <link rel="stylesheet" href="css/style.css">
	     <link rel="stylesheet" href="css/font-awesome.min.css">
    	<link rel="stylesheet" href="css/font-awesome.css">
	</head>
	<body >
		<nav class="navbar navbar-expand-lg navbar-light bg-light">

		    <div class="navbar-nav divnav">
		    	<span class="span1">
		    		<img class="navlogo" src="images/logoo.png" >
		    	</span>
		    	<a class='nav-item nav-link' href='admindashbord.php?bouton2=medecin'><b>medecin</b></a>
		    	<a class='nav-item nav-link' href='patient.php?bouton3=patient'><b>patient</b></a>
		    	<a class='nav-item nav-link active' href='#'><b>Profil</b></a>
				<a id="aout" class="nav-item nav-link" href='profil.php?bouton5=logout'><i class="fa fa-sign-out" aria-hidden="true"> </i></a>
		    </div>
		 </div>   
	</nav>
   <header class="hdr">
		<br>
		<center>
			<?php
			if (!empty($_GET['msg'])) {
				echo "<span class='spanmsg'>".$_GET['msg']."</span><br><br>";
			}
		?>
			<div class="divprofil">
				
				<div class="divimage">
					<img class="imageadmin" src="images/<?php echo $value['image']; ?>">
				</div>
				<div class="divinfo">
					<h3><b>Nom :</b><span id="nomadmin"> <?php echo $value['adminFirstName']; ?></span></h3>
					<h5><b>Email :</b><span id="emailadmin"> <?php echo $value['mail']; ?></span></h5>
					<button type="submit" class="btn bntprf btn-primary" onclick="modifier(<?php echo $value['id']; ?>)"><i class="fa fa-edit" aria-hidden="true"> </i> Modifier profil</button>
				</div>
			</div>
			<br>
			<div class="divprofil" id="divadmininfo" style="display: none;">
				<div class="divinfo1">
					<form method="post" action="Bloquer.php" enctype="multipart/form-data">
						<div class="form-group">
							<input type="hidden" name="id" value="<?php echo $value['id'] ?>">
							<label ><b>Nom :</b></label>
							<input type="text" class="form-control" name="nom" value="<?php echo $value['adminFirstName'] ?>">
							<br>
							<label ><b>Prenom :</b></label>
							<input type="text" class="form-control" name="pnom" value="<?php echo $value['adminLastName'] ?>">
							<br>
							<label for="exampleInputEmail1"><b>Email address :</b></label>
							<input type="email" class="form-control" name="email" value="<?php echo $value['mail'] ?>">
							<br>
							<label><b>Password :</b></label>
							<input type="password" class="form-control" name="password" value="<?php echo $value['password'] ?>">
							<br>
						</div>
						<div class="form-group ">
							<label >Image de profil :</label>
							<input type="file" class="form-control" name="image">
						</div>
						<button type="submit" name="modifieradmin" id="btttn1" value="Modifier" class="btn bbbb btn3 btn-primary"><i class="fa fa-edit" aria-hidden="true"> </i> Modifier</button>
					</form>
				</div>
			</div>
			<br><br>
		</center>
	</header>
	<script type="text/javascript">
		function modifier(x) {
			document.getElementById('divadmininfo').style="";
		}
			
	</script>
	</body>
</html>
<?php 
}
     if(!empty($_GET['bouton5'])){
		header('location:../index.php');
		session_destroy();
	}
 ?> 